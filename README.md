# Set Appointment Web Application

### Purpose
This project has written for our software engineering lab. This project can be
used in a real world university.

### Requirements
For this project we need python3.5 and django 1.11 or more. This project requires
pip or pip3 too.

### Installation
To install this project after install requirements just run:  
cd "project_dest"  
pip3 install requirements.txt  
python3.5 manage.py runserver  
