from django.db import models
from django.contrib.auth.models import User
import django.utils.timezone as time

# here we defined models in our database

# we defined student table that extends from user model
class Student(models.Model):
    student = models.OneToOneField(User, default=0, on_delete=models.CASCADE)
    orientation_choices = (
        ('graduate', 'Graduate'),
        ('under_graduate', 'Under Graduate'),
        ('doctrine', 'Doctrine'),
    )
    orientation = models.CharField(max_length=30, default='graduate', choices=orientation_choices)
    student_number = models.CharField(max_length=30, default='0000000')

    def __str__(self):
        return self.student_number

# we defined instructor table that extends from user model
class Instructor(models.Model):
    instructor = models.OneToOneField(User, default=0, on_delete=models.CASCADE)
    room_number = models.IntegerField(default=0)
    instructor_number = models.CharField(max_length=30, default='0000000')

    def __str__(self):
        return self.instructor_number

# this is our super user table
class Admin(models.Model):
    admin = models.OneToOneField(User, default=0, on_delete=models.CASCADE)

    def __str__(self):
        return 'Admin ' + self.admin.username

# room table for storing instructor times
class Room(models.Model):
    instructor_number = models.ForeignKey(Instructor, default=0)
    start_time = models.TimeField()
    start_date = models.DateField(default=time.now)
    end_time = models.TimeField()
    end_date = models.DateField(default=time.now)
    active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.instructor_number.room_number)

# reservation table for reserved times
class Reservation(models.Model):
    student_number = models.ForeignKey(Student, default=0)
    room_number = models.ForeignKey(Room, default=0)
    start_time = models.TimeField(default=time.now)
    start_date = models.DateField(default=time.now)
    end_time = models.TimeField()
    end_date = models.DateField(default=time.now)
    reserved = models.BooleanField(default=True)
    subject = models.CharField(max_length=500, default='')

    def __str__(self):
        return str(self.student_number)
