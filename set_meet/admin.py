from django.contrib import admin
from .models import Student, Instructor, Admin, Reservation, Room
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

class CustomUserAdmin(UserAdmin):
    add_fieldsets = (
      (None, {'fields':('username','password1','password2','first_name','last_name','email'),}),)


# admin models into the admin panel 
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)

admin.site.register(Admin)
admin.site.register(Student)
admin.site.register(Instructor)
admin.site.register(Reservation)
admin.site.register(Room)
# Register your models here.
