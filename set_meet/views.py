# libraries needed for our project
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout as django_logout, login
from .models import Instructor, Student, Room, Reservation
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime
from datetime import timedelta
from django.core.mail import send_mail
import calendar

# this is the functionality of login view
def login_view(request):
    # show login page
    if request.method == 'GET':
        return render(request, 'login/index.html')

    if request.method == 'POST':
        # authenticate user
        user = authenticate(username=request.POST['Username'], password=request.POST['Password'])
        room = Room.objects.all()
        days = set()
        for obj in room:
            if obj.start_date >= datetime.now().date() and obj.active:
                days.add(calendar.day_name[obj.start_date.weekday()]+' '+str(obj.start_date)+' for Dr.'+\
                    obj.instructor_number.instructor.last_name)

        if user is not None:
            login(request, user)
            try:
                student = Student.objects.get(student=user)
            except ObjectDoesNotExist:
                student = None
                pass

            try:
                instructor = Instructor.objects.get(instructor=user)
            except ObjectDoesNotExist:
                instructor = None
                pass

            if student is not None:
                masters = Instructor.objects.all()
                # show filter page for student
                return render(request, 'filter/index.html', {'master': masters, 'student': student, 'days': days})

            if instructor is not None:
                # show set appointments page for instructor
                return render(request, 'master-appointments/index.html', {'instructor': instructor})

        if user is None:
            return render(request, 'login/index.html', {'error': True})



# here we defined available_appointments page for student
def available_appointments(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            # stroring data posted in the form
            date = request.POST['Text']
            username = request.POST['Select']
            student = request.POST['student']
            parsed_date = datetime.strptime(date, "%m/%d/%Y").strftime("%Y-%m-%d")
            # check validity of time
            if parsed_date < str(datetime.now().date()):
                room = Room.objects.all()
                days = set()
                for obj in room:
                    if obj.start_date >= datetime.now().date():
                        days.add(calendar.day_name[obj.start_date.weekday()]+' '+str(obj.start_date)+' for Dr.'+\
                            obj.instructor_number.instructor.last_name)
                try:
                    student = Student.objects.get(student=request.user)
                except ObjectDoesNotExist:
                    student = None
                    pass
                masters = Instructor.objects.all()
                return render(request, 'filter/index.html', {'master': masters, 'student': student, 'days': days, 'error': True})

            try:
                instructor_id = Instructor.objects.get(instructor=username)
            except ObjectDoesNotExist:
                pass
            try:
                rooms = Room.objects.filter(start_date=parsed_date, instructor_number=instructor_id)
            except ObjectDoesNotExist:
                pass
            if rooms:
                return render(request, 'student-appointments/appointments.html', {'rooms': rooms, 'student': student})
            else:
                room = Room.objects.all()
                days = set()
                for obj in room:
                    if obj.start_date >= datetime.now().date():
                        days.add(calendar.day_name[obj.start_date.weekday()]+' '+str(obj.start_date)+' for Dr.'+\
                            obj.instructor_number.instructor.last_name)
                try:
                    student = Student.objects.get(student=request.user)
                except ObjectDoesNotExist:
                    student = None
                    pass
                masters = Instructor.objects.all()
                return render(request, 'filter/index.html', {'master': masters, 'student': student, 'days': days, 'error': True})

        else:
            # return 404 page
            return render(request, '404/index.html')
    else:
        return render(request, 'authentication/index.html')



# if student submit appointment this function handle it
def submit_appointment(request):
    if request.user.is_authenticated:
        room_id = request.POST['id']
        try:
            room = Room.objects.get(id=room_id)
        except ObjectDoesNotExist:
            pass
        student = request.POST['student']
        subject = request.POST['subject']
        try:
            student_instance = Student.objects.get(student_number=student)
        except ObjectDoesNotExist:
            pass
        # create an object and store it in model
        reserve = Reservation.objects.create(start_time=room.start_time, start_date=room.start_date, end_time=room.end_time, \
            end_date=room.end_date, room_number=room, student_number=student_instance, subject=subject)
        return render(request, 'submit-appointments/index.html')
    else:
        return render(request, 'authentication/index.html')



# this function will return a table that shows appointments
def reserved_list(request):
    if request.user.is_authenticated:
        reserve_list = Reservation.objects.all()
        return render(request, 'reserved-table/index.html', {'list': reserve_list})
    else:
        return render(request, 'authentication/index.html')


# this is the function that handles approve button in appointments table
def reserved_list2(request):
    if request.user.is_authenticated:
        data = request.POST['data']
        student = Student.objects.get(student_number=data)
        email = student.student.email
        # send email to student
        send_mail (
            'Appointment',
            'Your Appointment Approved',
            'Instructor@iut.ac.ir',
            [email],
            fail_silently=False,
        )
        time = int(request.POST['time'])

        reserve_obj = Reservation.objects.get(id=request.POST['id'])

        room_id = reserve_obj.room_number.id
        room = Room.objects.get(id=room_id)

        new_start_time = (datetime.combine(room.start_date, room.start_time) + timedelta(minutes=time)).time()
        if(new_start_time >= room.end_time):
            room.active = False
            room.save()

        else:
            room.start_time = new_start_time
            room.save()
            reserve_obj.reserved = False
            reserve_obj.save()

        reserve_list = Reservation.objects.all()
        return render(request, 'reserved-table/index.html', {'list': reserve_list})
    else:
        return render(request, 'authentication/index.html')


# when instructor submit some time this function will be loaded and will store data related to that
def set_time(request):
    if request.user.is_authenticated:
        start_time = request.POST['Time1']
        end_time = request.POST['Time2']
        date = request.POST['Text']
        instructor = request.POST['instructor']
        instructor_instance = Instructor.objects.get(instructor_number=instructor)
        parsed_date = datetime.strptime(date, "%m/%d/%Y").strftime("%Y-%m-%d")
        # create and object and store it in models
        Room.objects.create(instructor_number=instructor_instance, start_time=start_time, end_time=end_time, start_date=parsed_date \
            , end_date=parsed_date, active=True)
        return render(request, 'master-appointments/index.html', {'instructor': instructor})
    else:
        return render(request, 'authentication/index.html')


# logout function handler for student
def logout(request):
    django_logout(request)
    return render(request, 'login/index.html')


# logout function handler for master
def master_logout(request):
    django_logout(request)
    return render(request, 'login/index.html')

# function to return home page
def home(request):
    return render(request, 'home-page/index.html')



def error_404(request):
    return render(request, '404/index.html')
# Create your views here.
