# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-12-02 08:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('set_meet', '0022_auto_20171127_1141'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservation',
            name='reserved',
            field=models.BooleanField(default=False),
        ),
    ]
