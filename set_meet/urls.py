# we defined our urls in here
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.login_view, name='login page'),
    url(r'^appointments/$', views.available_appointments, name='appointments'),
    url(r'^submit/$', views.submit_appointment, name='submit'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^reservation/$', views.reserved_list, name='reserved list'),
    url(r'^set_time/$', views.set_time, name='instructor set time'),
    url(r'^reservations/$', views.reserved_list2, name='reserved list'),
    url(r'^master_logout/$', views.master_logout, name='master logout'),
]
